# Test Driven Development

## Table of Contents

- Overview
- Test Frameworks
- Test Doubles
- Refactoring
- Mocking
- Best practices
- Next Steps

---
## Overview

- FizzBuzz
- Stack
- Sort

## Test Frameworks

- [GoogleTest](https://github.com/google/googletest)
- [Catch](https://github.com/catchorg/Catch2)
- [Doctest](https://github.com/onqtam/doctest)
- [Boost](https://www.boost.org/doc/libs/1_43_0/libs/test/doc/html/index.html)


## Test Doubles

> Test doubles are objects that are used in unit tests as replacements
to the real production system collaborators.

### Overview

From the Test-Driven Development in C++ [5]:

> - **Dummy** - Objects that can be passed around as necessary but
do not have any type of test implementation and should never be used.
> - **Stubs** - These objects provide implementations with canned answers
that are suitable for the test.
> - **Spies** - These objects provide implementations that record the values
that were passed in so they can be used by the test.
> - **Mocks** - These objects are pre-programmed to expect specific calls and
parameters and can throw exceptions when necessary.
> - **Fake** - These objects generatlly have a simplified functional implementation
of a particular interface that is adquate for testing but not for production.

These definitions are taken from Martin Fowler's blog post [1].

> Meszaros uses the term Test Double as the generic term for any kind of pretend object used in place of a real object for testing purposes. The name comes from the notion of a Stunt Double in movies. (One of his aims was to avoid using any name that was already widely used.) Meszaros then defined five particular kinds of double:

> And there are five different types of doubles:

> - **Dummy** objects are passed around but never actually used. Usually they are just used to fill parameter lists.
> - **Stubs** provide canned answers to calls made during the test, usually not responding at all to anything outside what's programmed in for the test.
> - **Spies** are stubs that also record some information based on how they were called. One form of this might be an email service that records how many messages it was sent.
> - **Mocks** are what we are talking about here: objects pre-programmed with expectations which form a specification of the calls they are expected to receive.
> - **Fake** objects actually have working implementations, but usually take some shortcut which makes them not suitable for production (an in memory database is a good example).

### Examples

These examples are taken from The Little Mocker blog post [2].

Let's imagine we have this interface:

```java
interface Authorizer {
    public Boolean authorize(String username, String password);
}
```

and the following `System` to test:

```java
public class System {
    public System(Authorizer authorizer) {
        this.authorizer = authorizer;
    }

    public int loginCount() {
        //returns number of logged in users.
    }
}
```

#### Dummies

The following code is a **dummy** implementation of the `Authorizer` interface.

```java
public class DummyAuthorizer implements Authorizer {
    public Boolean authorize(String username, String password) {
        return null;
    }
}
```

**... because it does nothing. It has no business rules whatsoever. They allow you to pass a test as a collaborator *double*.**

```java
@Test
public void newlyCreatedSystem_hasNoLoggedInUsers() {
    System system = new System(new DummyAuthorizer());
    assertThat(system.loginCount(), is(0));
}
```

#### Stub

A **stub** is an implementation that returns hardcoded responses.

```java
public class AcceptingAuthorizerStub implements Authorizer {
    public Boolean authorize(String username, String password) {
        return true;
    }
}
```

**but to not implement any behavior apart from the behavior-under-test.**

#### Spies

A **spy** is an implementation that returns hardcoded responses **and
saves the state changes in order to be asserted in the test**

```java
public class AcceptingAuthorizerSpy implements Authorizer {
    public boolean authorizeWasCalled = false;

    public Boolean authorize(String username, String password) {
        authorizeWasCalled = true;
        return true;
    }
}

public class System {
    public System(Authorizer authorizer) {
        this.authorizer = authorizer;
    }

    public int loginCount() {
        //returns number of logged in users.
    }

    public bool login(String username, String password) {
        return this.authorizer.authorize(username, password);
    }
}

@Test
public void newlyCreatedSystem_hasNoLoggedInUsers() {
    Authorizer testAuthorizer = new AcceptingAuthorizerSpy();
    System system = new System(testAuthorizer);
    assertThat(system.loginCount(), is(0));
    system.login(); // should call authorize method
    assertThat(testAuthorizer.authorizedWasCalled, is(true));
}

```

#### Mocks

A **mock** is an implementation that tests against behavior expectations.

```java
public class AcceptingAuthorizerVerificationMock implements Authorizer {
    public boolean authorizeWasCalled = false;

    public Boolean authorize(String username, String password) {
        authorizeWasCalled = true;
        return true;
    }

    public boolean verify() {
        return authorizedWasCalled;
    }
}


@Test
public void newlyCreatedSystem_hasNoLoggedInUsers() {
    Authorizer mockAuthorizer = new AcceptingAuthorizerVerificationMock();
    System system = new System(mockAuthorizer);
    assertThat(system.loginCount(), is(0));
    system.login(); // should call authorize method
    assertThat(mockAuthorizer.verify(), is(true));
}
```

#### Fakes

A **fake** is an implementation of business rules tailored to the test case.
It should run 

```java
public class AcceptingAuthorizerFake implements Authorizer {
    public Boolean authorize(String username, String password) {
        return username.equals("Bob");
    }
}
```

## Mocking Frameworks

- Google Mock
- Trompleoeil
- Fake It

## Patterns for TDD
- Test Driven Development Patterns
- Red Bar Patterns
- Testing Patterns
- Green Bar Tests
- Design Patterns
- Refactoring

## Best Practices
...

## DEMO

## Next Steps

- Behavior-Driven Development

### Refereces

- [1] [Mocks Aren't Stubs](https://www.martinfowler.com/articles/mocksArentStubs.html)
- [2] [The Little Mocker
](https://blog.cleancoder.com/uncle-bob/2014/05/14/TheLittleMocker.html)
- [3] [Endo-Testing: Unit Testing with Mock Objects](https://www2.ccs.neu.edu/research/demeter/related-work/extreme-programming/MockObjectsFinal.PDF)
- [4] [Mocks vs Stubs vs Fakes In Unit Testing](https://dotnetcoretutorials.com/2021/06/19/mocks-vs-stubs-vs-fakes-in-unit-testing/)
- [5] [Test-Driven Development in C++](https://www.linkedin.com/learning/test-driven-development-in-c-plus-plus/test-doubles-overview)


- https://github.com/FreeYourSoul/FSeam
- Growing Object Oriented Software guided by Tests
- Working effectively with legacy code
