// Pre-Compiled Headers
#include "StandardLibrary.hpp"

// Test Framework
#include <gtest/gtest.h>

std::size_t findMidpoint(std::size_t l, std::size_t r) {
	return l + (r - l) / 2;
}

/// ===========================================================================
/// @brief Implements Binary Search Algorithm
/// ===========================================================================
class BinarySearcher {
  public:
	class InvalidArray : public std::runtime_error {
	  public:
		InvalidArray(const char *data) : std::runtime_error(data) {}
	};
	class OutOfOrderArray : public std::runtime_error {
	  public:
		OutOfOrderArray(const char *data) : std::runtime_error(data) {}
	};

	BinarySearcher(long *data, std::size_t size) : mData(data), mSize(size) {
		if (data == nullptr) {
			throw InvalidArray("Null Pointer Exception!");
		}
	}

	virtual ~BinarySearcher() = default;

	void validate() {
		for (auto i = 0U; i < mSize - 1; i++) {
			if (mData[i] > mData[i + 1]) {
				throw OutOfOrderArray("Unsorted Array Exception!");
			}
		}
	}

	bool find(int element) {
		auto lowerBound = findLowerBound(element);
		return lowerBound < mSize && mData[lowerBound] == element;
	}

	std::size_t findLowerBound(int element) {
		return findLowerBound(0, mSize, element);
	}

  protected:
	virtual std::size_t findLowerBound(std::size_t l, std::size_t r,
									   long element) {
		if (l == r) {
			return l;
		}
		auto midpoint = findMidpoint(l, r);
		if (element > mData[midpoint]) {
			l = midpoint + 1;
		} else {
			r = midpoint;
		}

		return findLowerBound(l, r, element);
	}

  private:
	long *mData;
	std::size_t mSize;
};

/// ===========================================================================
/// @brief Spies on the Binary Search Class
/// ===========================================================================
class BinarySearcherSpy : public BinarySearcher {
  public:
	int compares{0};

	BinarySearcherSpy(long *data, std::size_t size)
		: BinarySearcher(data, size) {}

  protected:
	std::size_t findLowerBound(std::size_t l, std::size_t r,
							   long element) override {
		compares++;
		return BinarySearcher::findLowerBound(l, r, element);
	}
};

/// ===========================================================================
/// @brief Tests the Binary Search Algorithm
/// ===========================================================================
class BinarySearchTest : public ::testing::Test {};

TEST_F(BinarySearchTest, createSearcher) {
	std::array<long, 2> numbers{};
	BinarySearcher searcher(numbers.data(), numbers.size());
}

TEST_F(BinarySearchTest, nullInputThrowsException) {
	EXPECT_THROW(BinarySearcher searcher(nullptr, 0),
				 BinarySearcher::InvalidArray);
}

TEST_F(BinarySearchTest, zeroSizedArrayThrowsException) {
	std::array<long, 0> numbers{};
	EXPECT_THROW(BinarySearcher searcher(numbers.data(), numbers.size()),
				 BinarySearcher::InvalidArray);
}

TEST_F(BinarySearchTest, checkInOrder) {
	std::vector<std::vector<long>> inOrderArrays{
		{0}, {0, 1}, {0, 1, 2, 3}, {0, 1, 1, 2, 3}};
	for (auto &inOrder : inOrderArrays) {
		BinarySearcher searcher(inOrder.data(), inOrder.size());
		searcher.validate();
	}
}

TEST_F(BinarySearchTest, outOfOrderThrowsExcepion) {
	std::vector<std::vector<long>> outOrderArrays{
		{1, 0}, {1, 0, 2}, {0, 2, 1}, {0, 1, 2, 4, 3}};
	std::size_t exceptions = 0;
	for (auto &outOfOrder : outOrderArrays) {
		BinarySearcher searcher(outOfOrder.data(), outOfOrder.size());
		try {
			searcher.validate();
		} catch (BinarySearcher::OutOfOrderArray &e) {
			exceptions++;
		}
	}
	EXPECT_EQ(outOrderArrays.size(), exceptions);
}

TEST_F(BinarySearchTest, findsProperMidpoint) {
	ASSERT_EQ(0, findMidpoint(0, 0));
	ASSERT_EQ(0, findMidpoint(0, 1));
	ASSERT_EQ(1, findMidpoint(0, 2));
	ASSERT_EQ(1, findMidpoint(0, 3));
	ASSERT_EQ(std::numeric_limits<long>::max() / 2,
			  findMidpoint(0, std::numeric_limits<long>::max()));
	ASSERT_EQ(std::numeric_limits<long>::max() / 2 + 1,
			  findMidpoint(1, std::numeric_limits<long>::max()));
	ASSERT_EQ(std::numeric_limits<long>::max() / 2 + 1,
			  findMidpoint(2, std::numeric_limits<long>::max()));
	ASSERT_EQ(std::numeric_limits<long>::max() / 2 + 2,
			  findMidpoint(3, std::numeric_limits<long>::max()));
	ASSERT_EQ(std::numeric_limits<long>::max() / 2,
			  findMidpoint(1, std::numeric_limits<long>::max() - 2));
}

void assertFound(int target, int index, std::initializer_list<long> domain) {
	std::vector<long> numbers(domain.begin(), domain.end());
	BinarySearcher searcher(numbers.data(), domain.size());
	ASSERT_TRUE(searcher.find(target));
	ASSERT_EQ(index, searcher.findLowerBound(target));
}

void assertNotFound(int target, int index, std::initializer_list<long> domain) {
	std::vector<long> numbers(domain.begin(), domain.end());
	BinarySearcher searcher(numbers.data(), domain.size());
	ASSERT_FALSE(searcher.find(target));
	ASSERT_EQ(index, searcher.findLowerBound(target));
}

TEST_F(BinarySearchTest, simpleFinds) {
	assertFound(0, 0, {0});
	assertFound(5, 2, {0, 1, 5, 7});
	assertFound(7, 3, {0, 1, 5, 7});
	assertFound(7, 5, {0, 1, 2, 2, 3, 7, 8});
	assertFound(2, 2, {0, 1, 2, 2, 3, 7, 8});
	assertFound(2, 2, {0, 1, 2, 2, 2, 3, 7, 8});

	assertNotFound(1, 1, {0});
	assertNotFound(6, 3, {1, 2, 5, 7, 9});
	assertNotFound(0, 0, {1, 2, 2, 5, 7, 9});
	assertNotFound(10, 6, {1, 2, 2, 5, 7, 9});
}

std::vector<long> makeArray(std::size_t n) {
	std::vector<long> result(n);
	std::iota(result.begin(), result.end(), 0);
	return result;
}

void assertCompares(int compares, std::size_t n) {
	auto numbers = makeArray(n);
	BinarySearcherSpy spy(numbers.data(), 0);
	spy.find(0);
	ASSERT_TRUE(spy.compares > 0);
	ASSERT_TRUE(spy.compares <= compares);
}

TEST_F(BinarySearchTest, logNCheck) {
	assertCompares(1, 1);
	assertCompares(5, 32);
	assertCompares(16, 65536);
}

/******************************************************************************
 * @brief Application entry point
 *****************************************************************************/
int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
