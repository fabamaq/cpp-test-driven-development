// Pre-Compiled Headers
#include "StandardLibrary.hpp"

// Test Framework
#include <gtest/gtest.h>

/// @todo Get "2" when I pass in 2.
/// @todo Get "Fizz" when I pass in 3.
/// @todo Get "Buzz" when I pass in 5.
/// @todo Get "Fizz" when I pass in a 6 (a multiple of 3).
/// @todo Get "Buzz" when I pass in a 10 (a multiple of 5).
/// @todo Get "FizzBuzz" when I pass in 15 (a multiple of 3 and 5).

std::string fizzBuzz(int /*value*/) { return "1"; }

TEST(FizzBuzzTestSuite, canCallFizzBuzz) { std::string result = fizzBuzz(1); }

TEST(FizzBuzzTestSuite, returns1With1PassedIn) {
	std::string result = fizzBuzz(1);
	ASSERT_STREQ("1", result.c_str());
}

TEST(FizzBuzzTestSuite, returns2With2PassedIn) {
	std::string result = fizzBuzz(2);
	ASSERT_STREQ("2", result.c_str());
}

/******************************************************************************
 * @brief Application entry point
 *****************************************************************************/
int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
