// Pre-Compiled Headers
#include "StandardLibrary.hpp"

// Test Framework
#include <gtest/gtest.h>

/// @todo Get "Fizz" when I pass in a 6 (a multiple of 3).
/// @todo Get "Buzz" when I pass in a 10 (a multiple of 5).
/// @todo Get "FizzBuzz" when I pass in 15 (a multiple of 3 and 5).

std::string fizzBuzz(int value) {
	if (3 == value) {
		return "Fizz";
	}
	if (5 == value) {
		return "Buzz";
	}
	return std::to_string(value);
}

void checkFizzBuzz(int value, std::string expectedResult) {
	std::string result = fizzBuzz(value);
	ASSERT_STREQ(expectedResult.c_str(), result.c_str());
}

TEST(FizzBuzzTestSuite, returns1With1PassedIn) { checkFizzBuzz(1, "1"); }

TEST(FizzBuzzTestSuite, returns2With2PassedIn) { checkFizzBuzz(2, "2"); }

TEST(FizzBuzzTestSuite, returnsFizzWith3PassedIn) { checkFizzBuzz(3, "Fizz"); }

/******************************************************************************
 * @brief Application entry point
 *****************************************************************************/
int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
