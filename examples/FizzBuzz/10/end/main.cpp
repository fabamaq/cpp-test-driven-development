// Pre-Compiled Headers
#include "StandardLibrary.hpp"

// Test Framework
#include <gtest/gtest.h>

std::string fizzBuzz(int value) {
	const auto isMultipleOf = [value](int base) {
		return (0 == (value % base));
	};
	if (isMultipleOf(15)) {
		return "FizzBuzz";
	}
	if (isMultipleOf(5)) {
		return "Buzz";
	}
	if (isMultipleOf(3)) {
		return "Fizz";
	}
	return std::to_string(value);
}

void checkFizzBuzz(int value, std::string expectedResult) {
	std::string result = fizzBuzz(value);
	ASSERT_STREQ(expectedResult.c_str(), result.c_str());
}

TEST(FizzBuzzTestSuite, returns1With1PassedIn) { checkFizzBuzz(1, "1"); }

TEST(FizzBuzzTestSuite, returns2With2PassedIn) { checkFizzBuzz(2, "2"); }

TEST(FizzBuzzTestSuite, returnsFizzWith3PassedIn) { checkFizzBuzz(3, "Fizz"); }

TEST(FizzBuzzTestSuite, returnsBuzzWith5PassedIn) { checkFizzBuzz(5, "Buzz"); }

TEST(FizzBuzzTestSuite, returnsFizzWith6PassedIn) { checkFizzBuzz(6, "Fizz"); }

TEST(FizzBuzzTestSuite, returnsBuzzWith10PassedIn) {
	checkFizzBuzz(10, "Buzz");
}

TEST(FizzBuzzTestSuite, returnsFizzBuzzWith15PassedIn) {
	checkFizzBuzz(15, "FizzBuzz");
}

/******************************************************************************
 * @brief Application entry point
 *****************************************************************************/
int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
