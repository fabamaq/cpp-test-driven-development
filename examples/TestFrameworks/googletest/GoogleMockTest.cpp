#include <gmock/gmock.h>
#include <gtest/gtest.h>
using ::testing::AtLeast;

/// ============================================================================
/// @file Turtle.hpp
/// ============================================================================
class Turtle {
  public:
	Turtle() = default;
	virtual ~Turtle() = default;
	virtual void PenUp() = 0;
	virtual void PenDown() = 0;
	virtual void Forward(int distance) = 0;
	virtual void Turn(int degrees) = 0;
	virtual void GoTo(int x, int y) = 0;
	virtual int GetX() const = 0;
	virtual int GetY() const = 0;
};

/// ===========================================================================
/// @file MockTurtle.hpp
/// ===========================================================================

#include <gmock/gmock.h>

class MockTurtle : public Turtle {
  public:
	MockTurtle() = default;
	~MockTurtle() = default;
	MOCK_METHOD(void, PenUp, (), (override));
	MOCK_METHOD(void, PenDown, (), (override));
	MOCK_METHOD(void, Forward, (int distance), (override));
	MOCK_METHOD(void, Turn, (int degrees), (override));
	MOCK_METHOD(void, GoTo, (int x, int y), (override));
	MOCK_METHOD(int, GetX, (), (const, override));
	MOCK_METHOD(int, GetY, (), (const, override));
};

/// ===========================================================================
/// @file Painter.hpp
/// ===========================================================================

// #include "Turtle.hpp" // forward declaration would be more efficient

class Painter {
  public:
	Painter(Turtle *newTurtle) : mTurtle(newTurtle) {}

	bool DrawCircle(int x, int y, int z) {
		(void)x;
		(void)y;
		(void)z;

		mTurtle->PenDown();

		return true;
	}

  private:
	Turtle *mTurtle;
};

/// ===========================================================================
/// @file PainterTest.cpp
/// ===========================================================================

TEST(PainterTest, CanDrawSomething) {
	MockTurtle turtle;

	/// @note expectations should be called before the calling the functions
	EXPECT_CALL(turtle, PenDown()).Times(AtLeast(1));

	Painter painter(&turtle);

	EXPECT_TRUE(painter.DrawCircle(0, 0, 10));
}

/******************************************************************************
 * @brief Application entry point
 *****************************************************************************/
int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
