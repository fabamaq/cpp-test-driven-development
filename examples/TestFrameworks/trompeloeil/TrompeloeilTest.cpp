#include "StandardLibrary.hpp"

#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"
// <catch.hpp> must be included before <trompeloeil.hpp>
#include "catch2/trompeloeil.hpp"

/// @brief class to mock
class store {
  public:
	virtual ~store() = default;
	virtual std::size_t inventory(const std::string &article) const = 0;
	virtual void remove(const std::string &article, std::size_t quantity) = 0;
};

/// @brief class to implement
class order {
  public:
	void add(const std::string name, std::size_t s) {
		article = name;
		quantity = s;
	}
	void fill(store &the_store) {
		if (the_store.inventory(article) >= quantity) {
			the_store.remove(article, quantity);
		}
	}

  private:
	std::string article;
	std::size_t quantity;
};

struct mock_store : public store {
  public:
	MAKE_CONST_MOCK1(inventory, std::size_t(const std::string &), override);
	MAKE_MOCK2(remove, void(const std::string &, std::size_t), override);
};

TEST_CASE("filling does nothing if stock is insufficient") {
	// create an order object
	order test_order;

	// save whiskies to order - no action
	test_order.add("Talisker", 51U);

	// create the mocked store - no action
	mock_store store;
	{
		const char *whisky = "Talisker";

		/**
		 * @brief setup expectation for call
		 * - creates an anonymous expectation object
		 * - expectation must be fulfilled before destruction
		 * of the expectation object at the end of the scope
		 * - return an expression covertible to the return type of the function
		 */
		REQUIRE_CALL(store, inventory(whisky)).RETURN(50U);

		test_order.fill(store);
	} // expectation end of scope
}

TEST_CASE("filling removes from store if in stock") {
	order test_order;
	test_order.add("Talisker", 50U);
	mock_store store;
	{
		trompeloeil::sequence seq;
		REQUIRE_CALL(store, inventory("Talisker")).RETURN(50U).IN_SEQUENCE(seq);
		REQUIRE_CALL(store, remove("Talisker", 50U)).IN_SEQUENCE(seq);
		test_order.fill(store);
	}
}

/// @sa https://accu.org/conf-docs/PDFs_2017/Bj%C3%B6rn_Fahller_Trompeloeil.pdf