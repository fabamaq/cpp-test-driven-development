/**
 * @file SortTest.cpp
 * @author Pedro Oliveira (pedro.andre@fabamaq.com)
 * @brief
 * @version 0.1
 * @date 2021-10-25
 *
 * @copyright Copyright (c) 2021
 *
 */

// Pre-Compiled Headers
#include "StandardLibrary.hpp"

// Test Framework
#include <gtest/gtest.h>

// ----------------------------------------------------------------------------
// Prints "data: <prefix> [ data[0], data[1], ..., data[N] ]"
// ----------------------------------------------------------------------------
void PrintVector(const std::vector<int> &data, const char *prefix = "") {
	std::stringstream ss{};
	ss << "data: " << prefix << " [ ";
	// printf("data: %s [ ", prefix);
	for (auto n : data) {
		// printf("%d ", n);
		ss << std::to_string(n) << " ";
	}
	ss << "]\n";
	std::cout << ss.str() << std::endl;
	// puts("]");
}

// ----------------------------------------------------------------------------
// Sorts data with a Naive algorithm
// ----------------------------------------------------------------------------
void NaiveSort(std::vector<int> &data) {
	if (data.size() < 2) {
		return;
	}

	for (auto i = 0U; i < data.size() - 1; i++) {
		for (auto j = i + 1; j < data.size(); j++) {
			if (data[i] > data[j]) {
				std::swap(data[i], data[j]);
			}
		}
	}
}

// NOLINTNEXTLINE
TEST(SortTestSuite, TestNaiveSort_withEmptyArray) {
	std::vector<int> numbers{};
	NaiveSort(numbers);
	EXPECT_EQ(std::vector<int>{}, numbers);
}

// NOLINTNEXTLINE
TEST(SortTestSuite, TestNaiveSort_withSingleValue) {
	std::vector<int> numbers{1};
	NaiveSort(numbers);
	EXPECT_EQ((std::vector<int>{1}), numbers);
}

// NOLINTNEXTLINE
TEST(SortTestSuite, TestNaiveSort_withTwoValues) {
	std::vector<int> numbers{1, 2};
	NaiveSort(numbers);
	EXPECT_EQ((std::vector<int>{1, 2}), numbers);
}

// NOLINTNEXTLINE
TEST(SortTestSuite, TestNaiveSort_withRandomValues) {
	std::vector<int> numbers{2, 1, 4, 6, 3};
	NaiveSort(numbers);
	EXPECT_EQ((std::vector<int>{1, 2, 3, 4, 6}), numbers);
}

// ----------------------------------------------------------------------------
// Sorts data with the Insertion Sort algorithm
// ----------------------------------------------------------------------------
void InsertionSortAsc(int *A, int length) {
	// for j = 2 to A.length
	for (int j = 1; j < length; j++) {
		// key = A[j]
		auto key = A[j];
		// Insert A[j] into the sorted sequence A[1 .. j - 1].
		int i = j - 1;
		// while i > 0 and A[i] > key
		while (i >= 0 && A[i] > key) {
			A[i + 1] = A[i];
			i = i - 1;
		}
		A[i + 1] = key;
	}
}

// NOLINTNEXTLINE
TEST(SortTestSuite, TestInsertionSort_withEmptyArray) {
	std::vector<int> numbers{};
	InsertionSortAsc(numbers.data(), static_cast<int>(numbers.size()));
	EXPECT_EQ(std::vector<int>{}, numbers);
}

// NOLINTNEXTLINE
TEST(SortTestSuite, TestInsertionSort_withSingleValue) {
	std::vector<int> numbers{1};
	InsertionSortAsc(numbers.data(), static_cast<int>(numbers.size()));
	EXPECT_EQ((std::vector<int>{1}), numbers);
}

// NOLINTNEXTLINE
TEST(SortTestSuite, TestInsertionSort_withTwoValues) {
	std::vector<int> numbers{1, 2};
	InsertionSortAsc(numbers.data(), static_cast<int>(numbers.size()));
	EXPECT_EQ((std::vector<int>{1, 2}), numbers);
}

TEST(SortTestSuite, TestInsertionSort_withRandomValues) {
	std::vector<int> numbers{2, 1, 4, 6, 3};
	InsertionSortAsc(numbers.data(), static_cast<int>(numbers.size()));
	std::vector<int> expected{1, 2, 3, 4, 6};
	ASSERT_EQ(expected.size(), numbers.size());
	for (auto i = 0U; i < numbers.size(); i++) {
		EXPECT_EQ(numbers[i], expected[i]);
	}
}

// ============================================================================
// Test Sorting Performance with timed tests
// ============================================================================
class SortPerformanceTest : public ::testing::Test {
  protected:
	void SetUp() override {
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<> distrib(1, 99999);
		numbers.resize(100000); // 100'000
		std::generate(numbers.begin(), numbers.end(),
					  [&]() { return distrib(gen); });
#ifndef NDEBUG
		PrintVector(numbers);
#endif
		start = std::chrono::high_resolution_clock::now();
	}

	void TearDown() override {
		auto end = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> diff = end - start;
#ifndef NDEBUG
		PrintVector(numbers);
		printf("Test took %f seconds\n", diff.count());
		ASSERT_LE(diff.count(), 20.0);
#else
		ASSERT_LE(diff.count(), 2.0);
#endif
	}

	std::vector<int> &get_numbers() { return numbers; }

  private:
	std::vector<int> numbers{};
	std::chrono::time_point<std::chrono::high_resolution_clock> start;
};

// NOLINTNEXTLINE
TEST_F(SortPerformanceTest, DISABLED_TestNaiveSort_withTooManyValues) {
	NaiveSort(get_numbers());
}

// NOLINTNEXTLINE
TEST_F(SortPerformanceTest, TestInsertionSort_withTooManyValues) {
	InsertionSortAsc(get_numbers().data(),
					 static_cast<int>(get_numbers().size()));
}

/******************************************************************************
 * @brief Application entry point
 *****************************************************************************/
int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
