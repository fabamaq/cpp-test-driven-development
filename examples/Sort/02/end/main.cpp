// Pre-Compiled Headers
#include "StandardLibrary.hpp"

// Test Framework
#include <gtest/gtest.h>

void Sort(std::vector<int> &data) {
	if (data.size() < 2) {
		return;
	}
	for (auto i = 0U; i < data.size() - 1; i++) {
		for (auto j = i + 1; j < data.size(); j++) {
			if (data[i] > data[j]) {
				std::swap(data[i], data[j]);
			}
		}
	}
}

TEST(SortTestSuite, AnEmptyArrayIsAlwaysSorted) {
	std::vector<int> numbers{};
	Sort(numbers);
	EXPECT_EQ(std::vector<int>{}, numbers);
}

TEST(SortTestSuite, ASortedArrayIsAlwaysSorted) {
	std::vector<int> numbers{1, 2};
	Sort(numbers);
	EXPECT_EQ((std::vector<int>{1, 2}), numbers);
}

TEST(SortTestSuite, AnUnsortedArrayGetsSorted) {
	std::vector<int> numbers{2, 1, 4, 6, 3};
	Sort(numbers);
	EXPECT_EQ((std::vector<int>{1, 2, 3, 4, 6}), numbers);
}

/******************************************************************************
 * @brief Application entry point
 *****************************************************************************/
int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
