get_filename_component(PROJECT_SUB_DIRECTORY ${CMAKE_CURRENT_LIST_DIR} NAME)

# =============================================================================
# Adds target for begin version of current project, current iteration exe
set(BEGIN_EXECUTABLE_TARGET_NAME
    test_${PROJECT_NAME}_${PROJECT_SUB_DIRECTORY}_begin
)
add_executable(${BEGIN_EXECUTABLE_TARGET_NAME})
target_sources(
  ${BEGIN_EXECUTABLE_TARGET_NAME}
  PRIVATE begin/main.cpp $<TARGET_OBJECTS:global_pre_compiled_headers>
)
target_include_directories(
  ${BEGIN_EXECUTABLE_TARGET_NAME} PRIVATE ${ROOT_DIR}/lib
)
target_link_libraries(
  ${BEGIN_EXECUTABLE_TARGET_NAME} PRIVATE global_compile_options gtest
                                          Threads::Threads
)
# Automatically add tests with CTest by querying the compiled test executable
# for available tests:
gtest_discover_tests(
  ${BEGIN_EXECUTABLE_TARGET_NAME}
  TEST_PREFIX "${PROJECT_DIRECTORY}_${PROJECT_SUB_DIRECTORY}_UnitTests_begin_"
)

# =============================================================================
# Adds target for end version of current project, current iteration exe
set(END_EXECUTABLE_TARGET_NAME
    test_${PROJECT_NAME}_${PROJECT_SUB_DIRECTORY}_end
)
add_executable(${END_EXECUTABLE_TARGET_NAME})
target_sources(
  ${END_EXECUTABLE_TARGET_NAME}
  PRIVATE end/main.cpp $<TARGET_OBJECTS:global_pre_compiled_headers>
)
target_include_directories(
  ${END_EXECUTABLE_TARGET_NAME} PRIVATE ${ROOT_DIR}/lib
)
target_link_libraries(
  ${END_EXECUTABLE_TARGET_NAME} PRIVATE global_compile_options gtest
                                        Threads::Threads
)
# Automatically add tests with CTest by querying the compiled test executable
# for available tests:
gtest_discover_tests(
  ${END_EXECUTABLE_TARGET_NAME}
  TEST_PREFIX "${PROJECT_DIRECTORY}_${PROJECT_SUB_DIRECTORY}_UnitTests_end_"
)
