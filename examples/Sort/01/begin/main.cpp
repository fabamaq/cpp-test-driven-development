// Pre-Compiled Headers
#include "StandardLibrary.hpp"

// Test Framework
#include <gtest/gtest.h>

void Sort(std::vector<int> &data) { (void)data; }

TEST(SortTestSuite, AnEmptyArrayIsAlwaysSorted) {
	std::vector<int> numbers{};
	Sort(numbers);
	EXPECT_EQ(std::vector<int>{}, numbers);
}

TEST(SortTestSuite, ASortedArrayIsAlwaysSorted) {
	std::vector<int> numbers{1, 2};
	Sort(numbers);
	EXPECT_EQ((std::vector<int>{1, 2}), numbers);
}

TEST(SortTestSuite, AnUnsortedArrayGetsSorted) {
	std::vector<int> numbers{2, 1};
	Sort(numbers);
	EXPECT_EQ((std::vector<int>{1, 2}), numbers);
}

/******************************************************************************
 * @brief Application entry point
 *****************************************************************************/
int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
