// Pre-Compiled Headers
#include "StandardLibrary.hpp"

// Test Framework
#include <gtest/gtest.h>

// ----------------------------------------------------------------------------
// Prints "data: <prefix> [ data[0], data[1], ..., data[N] ]"
// ----------------------------------------------------------------------------
void PrintVector(const std::vector<int> &data, const char *prefix = "") {
	std::stringstream ss{};
	ss << "data: " << prefix << " [ ";
	// printf("data: %s [ ", prefix);
	for (auto n : data) {
		// printf("%d ", n);
		ss << std::to_string(n) << " ";
	}
	ss << "]\n";
	std::cout << ss.str() << std::endl;
	// puts("]");
}

// ----------------------------------------------------------------------------
// Sorts data with a slow algorithm
// ----------------------------------------------------------------------------
void NaiveSort(std::vector<int> &data) {
	if (data.size() < 2) {
		return;
	}

	for (auto i = 0U; i < data.size() - 1; i++) {
		for (auto j = i + 1; j < data.size(); j++) {
			if (data[i] > data[j]) {
				std::swap(data[i], data[j]);
			}
		}
	}
}

// NOLINTNEXTLINE
TEST(SortTestSuite, AnEmptyArrayIsAlwaysSorted) {
	std::vector<int> numbers{};
	NaiveSort(numbers);
	EXPECT_EQ(std::vector<int>{}, numbers);
}

// NOLINTNEXTLINE
TEST(SortTestSuite, ASingleValueIsAlwaysSorted) {
	std::vector<int> numbers{1};
	NaiveSort(numbers);
	EXPECT_EQ((std::vector<int>{1}), numbers);
}

// NOLINTNEXTLINE
TEST(SortTestSuite, ASortedArrayIsAlwaysSorted) {
	std::vector<int> numbers{1, 2};
	NaiveSort(numbers);
	EXPECT_EQ((std::vector<int>{1, 2}), numbers);
}

// NOLINTNEXTLINE
TEST(SortTestSuite, AnUnsortedArrayGetsSorted) {
	std::vector<int> numbers{2, 1, 4, 6, 3};
	NaiveSort(numbers);
	EXPECT_EQ((std::vector<int>{1, 2, 3, 4, 6}), numbers);
}

// ============================================================================
// Test Sorting Performance with timed tests
// ============================================================================
class SortPerformanceTest : public ::testing::Test {
  protected:
	void SetUp() override {
		std::random_device rd;
		std::mt19937 gen(rd());
		std::uniform_int_distribution<> distrib(1, 99999);
		numbers.resize(100000); // 100'000
		std::generate(numbers.begin(), numbers.end(),
					  [&]() { return distrib(gen); });
#ifndef NDEBUG
		PrintVector(numbers);
#endif
		start = std::chrono::high_resolution_clock::now();
	}

	void TearDown() override {
		auto end = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> diff = end - start;
#ifndef NDEBUG
		PrintVector(numbers);
		printf("Test took %f seconds\n", diff.count());
		ASSERT_LE(diff.count(), 20.0);
#else
		ASSERT_LE(diff.count(), 2.0);
#endif
	}

	std::vector<int> &get_numbers() { return numbers; }

  private:
	std::vector<int> numbers{};
	std::chrono::time_point<std::chrono::high_resolution_clock> start;
};

// NOLINTNEXTLINE
TEST_F(SortPerformanceTest, DISABLED_AnUnsortedArrayGetsSorted_fast) {
	NaiveSort(get_numbers());
}

/******************************************************************************
 * @brief Application entry point
 *****************************************************************************/
int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
