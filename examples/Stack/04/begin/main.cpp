// Pre-Compiled Headers
#include "StandardLibrary.hpp"

// Test Framework
#include <gtest/gtest.h>

class Stack {
  public:
	bool isEmpty() { return mIsEmpty; }

	void push(int /*element*/) { mIsEmpty = false; }

  private:
	bool mIsEmpty{true};
};

class TestingStack : public ::testing::Test {
  protected:
	Stack stack;
};
TEST_F(TestingStack, NewStack_IsEmpty) { ASSERT_TRUE(stack.isEmpty()); }

TEST_F(TestingStack, AfterOnePush_IsNotEmpty) {
	stack.push(0);
	ASSERT_FALSE(stack.isEmpty());
}

TEST_F(TestingStack, WillThrowUnderflow_WhenEmptyStackIsPopped) {
	ASSERT_THROW(stack.pop(), Stack::Underflow);
}

/******************************************************************************
 * @brief Application entry point
 *****************************************************************************/
int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
