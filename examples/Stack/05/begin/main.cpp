// Pre-Compiled Headers
#include "StandardLibrary.hpp"

// Test Framework
#include <gtest/gtest.h>

class Stack {
  public:
	class Underflow : public std::runtime_error {
	  public:
		Underflow(const char *msg) : std::runtime_error(msg) {}
	};

	bool isEmpty() { return mIsEmpty; }

	void push(int /*element*/) { mIsEmpty = false; }

	int pop() {
		throw Underflow("Empty Stack");
		return -1;
	}

  private:
	bool mIsEmpty{true};
};

class TestingStack : public ::testing::Test {
  protected:
	Stack stack;
};
TEST_F(TestingStack, NewStack_IsEmpty) { ASSERT_TRUE(stack.isEmpty()); }

TEST_F(TestingStack, AfterOnePush_IsNotEmpty) {
	stack.push(0);
	ASSERT_FALSE(stack.isEmpty());
}

TEST_F(TestingStack, WillThrowUnderflow_WhenEmptyStackIsPopped) {
	ASSERT_THROW(stack.pop(), Stack::Underflow);
}

TEST_F(TestingStack, AfterOnePushAndOnePop_WillBeEmpty) {
	stack.push(0);
	stack.pop();
	ASSERT_TRUE(stack.isEmpty());
}

/******************************************************************************
 * @brief Application entry point
 *****************************************************************************/
int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
