// Pre-Compiled Headers
#include "StandardLibrary.hpp"

// Test Framework
#include <gtest/gtest.h>

class Stack {
  public:
	class Underflow : public std::runtime_error {
	  public:
		Underflow(const char *msg) : std::runtime_error(msg) {}
	};

	bool isEmpty() { return mSize == 0; }

	void push(int /*element*/) { mSize++; }

	int pop() {
		if (isEmpty()) {
			throw Underflow("Empty Stack");
		}
		mSize--;
		return -1;
	}

  private:
	int mSize{0};
};

class TestingStack : public ::testing::Test {
  protected:
	Stack stack;
};
TEST_F(TestingStack, NewStack_IsEmpty) { ASSERT_TRUE(stack.isEmpty()); }

TEST_F(TestingStack, AfterOnePush_IsNotEmpty) {
	stack.push(0);
	ASSERT_FALSE(stack.isEmpty());
}

TEST_F(TestingStack, WillThrowUnderflow_WhenEmptyStackIsPopped) {
	ASSERT_THROW(stack.pop(), Stack::Underflow);
}

TEST_F(TestingStack, AfterOnePushAndOnePop_WillBeEmpty) {
	stack.push(0);
	stack.pop();
	ASSERT_TRUE(stack.isEmpty());
}

TEST_F(TestingStack, AfterTwoPushesAndOnePop_WillNotBeEmpty) {
	stack.push(0);
	stack.push(0);
	stack.pop();
	ASSERT_FALSE(stack.isEmpty());
}

TEST_F(TestingStack, AfterPushingX_WillPopX) {
	stack.push(99);
	int result = stack.pop();
	ASSERT_EQ(99, result);
	stack.push(88);
	result = stack.pop();
	ASSERT_EQ(88, result);
}

/******************************************************************************
 * @brief Application entry point
 *****************************************************************************/
int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
