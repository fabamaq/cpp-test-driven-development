// Pre-Compiled Headers
#include "StandardLibrary.hpp"

// Test Framework
#include <gtest/gtest.h>

class Stack {
  public:
	bool isEmpty() { return mIsEmpty; }

	void push(int /*element*/) { mIsEmpty = false; }

  private:
	bool mIsEmpty{true};
};

TEST(StackTestSuite, CreateStack) {
	Stack stack;
	ASSERT_TRUE(stack.isEmpty());
}

TEST(StackTestSuite, Push) {
	Stack stack;
	stack.push(0);
	ASSERT_FALSE(stack.isEmpty());
}

/******************************************************************************
 * @brief Application entry point
 *****************************************************************************/
int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
