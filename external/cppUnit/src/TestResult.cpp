#include "TestResult.h"

TestResult::TestResult() : runCount(0), errorCount(0) {}

void TestResult::testStarted() {
    runCount++;
}

void TestResult::testFailed() {
    errorCount++;
}

std::string TestResult::summary() {
    return std::to_string(runCount) + " run, " + std::to_string(errorCount) + " failed";
}
