#include "TestCase.h"

TestCase::TestCase (void (TestCase::* test)() ) : test(test) {}

void TestCase::run(TestResult & result) {
    result.testStarted();
    this->setUp();
    try {
        std::ref(test)(this);
    }
    catch (int e) {
        result.testFailed();
    }
    this->tearDown();
}
