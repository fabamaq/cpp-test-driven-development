#include "Assert.h"

#include <iostream>

void reportAssertionFailure(const char* expr, const char* file, int line) {
    std::cout << "Assertion Failure:" << std::endl;
    std::cout << "File: " << file << std::endl;
    std::cout << "Line: " << line << std::endl;
    std::cout << "Expr: " << expr << std::endl;
}
