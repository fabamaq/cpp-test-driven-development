main()
{
	echo "Building..."
	if [ ! -d "bin" ] ; then
        mkdir bin
    fi
	cmake .
	wait
	if [ ! -d "lib" ] ; then
        mkdir lib
    fi
	wait
	make cppUnit-shared
	wait
	mv libcppUnit-shared.so lib
	wait
	make cppUnit-static
	wait
	mv libcppUnit-static.a lib
	echo "Done."
}

main
