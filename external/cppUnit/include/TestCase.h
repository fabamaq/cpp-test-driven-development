#ifndef TEST_CASE_H
#define TEST_CASE_H

#include <functional>

#include "TestResult.h"

class TestCase {
	void (TestCase::* test)();
public:
	TestCase (void (TestCase::* test)() );

	virtual void setUp() {}
	virtual void tearDown() {}
	virtual void run(TestResult & result);
};

#endif // TEST_CASE_H
