#ifndef TEST_SUITE_H
#define TEST_SUITE_H

#include <vector>

#include "TestCase.h"
#include "TestResult.h"

class TestSuite {
	std::vector<TestCase*> tests;
public:
	TestSuite();

	void add(TestCase * test);
	void run(TestResult & result);
};

#endif // TEST_SUITE_H