#pragma once

// Assertion Implementation from Game Engine Architecture
#if ASSERTIONS_ENABLED

/*
 * define some inline assembly that causes a break
 * into the debugger -- this will be different on
 * each target CPU'
 */
#define debugBreak() asm ( "syscall" )

void reportAssertionFailure(const char* expr, const char* file, int line);

// check the expression and fail if it is false
#define ASSERT(expr)    \
    if (expr) { }   \
    else    \
    {   \
        reportAssertionFailure(#expr,   \
                   __FILE__, __LINE__); \
        debugBreak();   \
    }

#else

#define ASSERT(expr)    // evaluates to nothing

#endif
