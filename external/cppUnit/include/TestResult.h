#ifndef TEST_RESULT_H
#define TEST_RESULT_H

#include <string>

class TestResult {
	int runCount;
	int errorCount;
public:
	TestResult();

	void testStarted();
	void testFailed();

	std::string summary();
};

#endif // TEST_RESULT_H