main()
{
	if [ "$1" ]; then
		TEST_DIR=$1
	else
		TEST_DIR=.
	fi

	TESTS=`ls -la ${TEST_DIR} | grep -v "builder\|runner.sh" | grep --color=auto "^-.*\-x.*" | awk '{print $NF}'`
	# echo "$TESTS"
	for TEST in $TESTS
	do
		echo "Testing ${TEST_DIR}/$TEST"
		"${TEST_DIR}/$TEST"
	done
}

main $@
