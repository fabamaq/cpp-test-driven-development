#include <cassert>
#include <exception>
#include <functional>
#include <iostream>
#include <string>
#include <vector>

#include "Assert.h"
#include "TestResult.h"
#include "TestCase.h"
#include "TestSuite.h"

class WasRun : public TestCase {
public:
	int wasRun;
	std::string log;

	WasRun( void (WasRun::* test)() ) : TestCase (
		static_cast< void (TestCase::*)() > (test)
	) {};

	void testMethod() {
		wasRun = 1;
		log += "testMethod ";
	}

	void testBrokenMethod() {
		throw -1;
	}

	void setUp() override {
		wasRun = 0;
		log = "setUp ";
	}

	void tearDown() override {
		log += "tearDown ";
	}
};

class TestCaseTest: public TestCase {
public:
	TestResult result = TestResult();

	TestCaseTest( void (TestCaseTest::* test)() ) : TestCase (
		static_cast< void (TestCase::*)() > (test)
	) {};

	void setUp() override {
		result = TestResult();
	}

	void testTemplateMethod() {
		WasRun test = WasRun( &WasRun::testMethod );
		test.run(this->result);
		ASSERT(test.log.compare("setUp testMethod tearDown") == 0);
	}

	void testResult() {
		WasRun test = WasRun( &WasRun::testMethod );
		test.run(this->result);
		assert(this->result.summary().compare("1 run, 0 failed") == 0);
	}

	void testFailedResult() {
		WasRun test = WasRun( &WasRun::testBrokenMethod );
		test.run(this->result);
		assert(this->result.summary().compare("1 run, 1 failed") == 0);
	}

	void testFailedResultFormatting() {
		this->result.testStarted();
		this->result.testFailed();
		assert(this->result.summary().compare("1 run, 1 failed") == 0);
	}

	void testSuite() {
		TestSuite suite = TestSuite();
		suite.add( new WasRun( &WasRun::testMethod ));
		suite.add( new WasRun( &WasRun::testBrokenMethod ));
		suite.run(this->result);
		assert(this->result.summary().compare("2 run, 1 failed") == 0);
	}
};

int main() {
	TestSuite suite = TestSuite();
	suite.add( new TestCaseTest( &TestCaseTest::testTemplateMethod ) );
	suite.add( new TestCaseTest( &TestCaseTest::testResult ) );
	suite.add( new TestCaseTest( &TestCaseTest::testFailedResultFormatting ) );
	suite.add( new TestCaseTest( &TestCaseTest::testFailedResult ) );
	suite.add( new TestCaseTest( &TestCaseTest::testSuite ) );
	TestResult result = TestResult();
	suite.run(result);
	std::cout << result.summary() << std::endl;
    return 0;
}
