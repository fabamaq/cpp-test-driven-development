#include <cassert>
#include <iostream>

#include "TestCase.h"
#include "TestResult.h"
#include "TestSuite.h"

class IntVal {
    int intVal;
public:
    IntVal() : intVal(0) {}
    IntVal(int val) : IntVal() {
        setIntVal(val);
    }
    
    int getIntVal() const { return intVal; }
    void setIntVal(const int & val) { intVal = val; }
};

class IntValTest : public TestCase {
public:
    IntValTest( void (IntValTest::* test)() ) : TestCase (
		static_cast< void (TestCase::*)() > (test)
	) {};

    void testIntVal() {
        IntVal intVal = IntVal(2);
        assert(intVal.getIntVal() == 2);
    }
};

int main() {
    IntValTest ivTest = IntValTest( &IntValTest::testIntVal );
    TestResult result = TestResult();
    ivTest.run(result);
    std::cout << result.summary() << std::endl;
    return 0;
}
