cmake_minimum_required(VERSION 3.1)
project (cppUnit-test)

if (CMAKE_VERSION VERSION_LESS "3.1")
  add_definitions(-std=c++11)
else()
  set(CMAKE_CXX_STANDARD 11)
  set(CMAKE_CXX_STANDARD_REQUIRED ON)
  if(NOT CYGWIN)
    set(CMAKE_CXX_EXTENSIONS OFF)
  endif()
endif()

# Macro definitions
add_definitions(-DASSERTIONS_ENABLED)

#For the shared library:
set ( PROJECT_LIBRARY_DIR ../lib )
find_library ( STATIC_LIB libcppUnit-static.a ${PROJECT_LIBRARY_DIR} )
set ( EXTRA_LIBS ${EXTRA_LIBS} ${STATIC_LIB})
#link_directories( ~/ei06125/pt/up/fe/mieic/tvvs/tddByExample/cppUnit/lib )

include_directories(../include)

add_executable(cppUnit-test cppUnit-test.cpp ${SOURCES})
target_link_libraries(cppUnit-test ${EXTRA_LIBS} )

add_executable(sample-test sample-test.cpp ${SOURCES})
target_link_libraries(sample-test ${EXTRA_LIBS} )
