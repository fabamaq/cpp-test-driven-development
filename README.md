# C++ Test-Driven Development KS

A C++ Test-Driven Development Knowledge Sharing experience.

---
## DISCLAIMER
> This project is under constructions.
---

## Download

Download the zip archive and extract the files or clone the project:

```bash
  git clone --recurse-submodules git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-test-driven-development.git
```

### Build Requirements

* [g++](https://gcc.gnu.org/) (4.8 or higher)
* [cmake](https://cmake.org/) (3.20.0 or higher)
* * [make](https://www.gnu.org/software/make/) (4.2.1 or higher)

### Build Process

With `cmake`

```bash
# From the root directory

# Generate the build system
# run cmake . -B <build_directory> -D CMAKE_BUILD_TYPE=[Debug|Release]
# Example
cmake . -B build/Debug/ -D CMAKE_BUILD_TYPE=Debug

# Build the project
# run cmake --build <build_directory> --target <TARGET>
# Example
cmake --build/Debug --target MazeGame
```

Or you can setup your Integrated Development Environment (IDE) with cmake but, instructions for that, are outside the scope of this README (which can be updated with images if you want).

## Documentation

The docs/ folder contains some insights about Test-Driven Development process and best practices.

## Examples Projects

The examples folder contains examples of TDD exercises:

### Fizz Buzz

This exercise is the standard example of TDD.

### Stack & Sort

This exercise helps to transfer knowledge of TDD to more complex structures, such as Containers and Algorithms.

### Test Frameworks

This folder contains sample code that integrates with several Test Frameworks like CatchV2, GoogleTest, DocTest, etc..

## External Projects

At the moment, the external folder contains the Google Test Framework only, which is added to cmake automatically.

## Authors

- [Pedro André Oliveira](https://www.linkedin.com/in/pedroandreoliveira/)

## Contributing

Contributions are always welcome!

## Related Knowledge Sharings

* [C++ Test-Driven Development](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-test-driven-development.git)
* [C++ Design Patterns KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-design-patterns.git)
* [C++ Clean Architecture KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-clean-architecture.git)

* [C++ Fundamentals KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-fundamentals.git)
* [C++ Intermediate KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-intermediate.git)
* [C++ Advanced KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-advanced.git)
* [C++ Mastery KS](git@gitlab.pt.fabamaq.com:knowledge-sharings/cppseries/internal/cpp-mastery.git)

## References

Some good books about Test-Driven Development and Continuous Integration best practices:

* [Test-Driven Development](https://www.amazon.com/Test-Driven-Development-Kent-Beck/dp/0321146530/ref=sr_1_1?keywords=test-driven+development&qid=1638267528&sr=8-1)
* [Growing Object-Oriented Software, Guided by Tests](https://www.amazon.com/Growing-Object-Oriented-Software-Guided-Tests/dp/0321503627)
* [Unit Testing Principles, Practices, and Patterns](https://www.amazon.com/Unit-Testing-Principles-Practices-Patterns/dp/1617296279)
* [Continuous Integration: Improving Software Quality and Reducing Risk](https://www.amazon.com/Continuous-Integration-Improving-Software-Reducing/dp/0321336380/)
* [Continuous Delivery](https://www.amazon.com/Continuous-Delivery-Deployment-Automation-Addison-Wesley/dp/0321601912)
